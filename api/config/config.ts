/*import * as config from './config.json';
let env = (process.env.NODE_ENV) ? process.env.NODE_ENV : 'localhost';
import * as dotenv from 'dotenv';
export class ConfigProcess {
    constructor(){
        this.initial();
    }
    private initial(){
        if(config[env]){
            for(let idx in config[env]){
                process.env[idx] = config[env][idx];
            } 
        }
    }
}
*/
import * as dotenv from 'dotenv';
import * as appRoot from 'app-root-path';
import * as fs from "fs";
export class ConfigProcess {
    constructor(){
        //this.initial();
        const envConfig = dotenv.parse(fs.readFileSync(`${appRoot}/env`));
        this.initial(envConfig);
    }
    private initial(config){
        for (const k in config) {
            process.env[k] = config[k]
        }
    }
}
