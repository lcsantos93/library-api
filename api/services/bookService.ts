import { injectable, inject } from "inversify";
import { Logger } from "../components/logger/logger";
import { MongoDb } from "../components/mongodb/mongodb";

@injectable()
export class BookService {
  constructor(
    @inject(Symbol.for('Logger')) private logger: Logger,
    @inject(Symbol.for('MongoDb')) private mongo: MongoDb
  ) {
    this.mongo = mongo;
  }

  private sendBooktoDb = (req, connection) => {
    let book = {
      title: req.body.title,
      isbn: req.body.isbn,
      category: req.body.category,
      year: req.body.year
    };

    connection.create(book, err => {
      if (err) {
        this.logger.error(`[METHOD: sendBookToDb] >> Failed to save db ${err}`);
      } else {
        this.mongo.getCollection('Books').create(book);
      }
    })
  }
  
  private getAllBooks = async (res, connection) => {
    let books = await connection.find({});
    this.logger.info(`Books returned on getMethod: ${books}`);
    books == null ? 'Não há livros cadastrados' : books;
    return res['books'] = books;
  }
  public async addBook(req) {
    let obj = this.mongo.getCollection('Books');
    this.logger.info(`Obj returned of getCollection method: ${obj}`);
    this.sendBooktoDb(req, obj);
  }

  public listBooks = async (req) => {
    let obj = this.mongo.getCollection('Books');    
    await this.getAllBooks(req, obj);
  }

}