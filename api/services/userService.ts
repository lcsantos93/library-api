import { injectable, inject } from "inversify";
import { Logger } from "../components/logger/logger";
import { MongoDb } from "../components/mongodb/mongodb";
// import * as Bcrypt from "bcrypt";
@injectable()
export class UserService {
  constructor(
    @inject(Symbol.for('Logger')) private logger: Logger,
    @inject(Symbol.for('MongoDb')) private mongo: MongoDb
    // @inject(Symbol.for('Bcrypt')) private bcrypt: Bcrypt
  ) {
    this.mongo = mongo;
    // this.bcrypt = bcrypt;
  }
  private hashPass = (pass) => {
    // this.bcrypt.hast(pass, 10, (err, hash) => {
    //   return hash
    // })
  }
  private sendUsertoDb = (req, connection) => {
    let pass = this.hashPass(req.body.password);
    let user = {
      name: req.body.name,
      age: req.body.age,
      phone: req.body.phone,
      email: req.body.email,
      password: req.body.password
    };

    connection.create(user, err => {
      if (err) {
        this.logger.error(`[METHOD: sendUserToDb] >> Failed to save db ${err}`);
      } else {
        this.mongo.getCollection('Users').create(user);
      }
    })
  }

  private getAllUsers = async (res, connection) => {
    let users = await connection.find({});
    this.logger.info(`Users returned on getMethod: ${users}`);
    users == null ? 'Não há Usuários cadastrados' : users;
    return res['users'] = users;
  }

  public async addUser(req) {
    let obj = this.mongo.getCollection('Users');
    this.logger.info(`Obj returned of getCollection method: ${obj}`);
    this.sendUsertoDb(req, obj);
  }

  public listUsers = async (req) => {
    let obj = this.mongo.getCollection('Users');
    await this.getAllUsers(req, obj);
  }

}