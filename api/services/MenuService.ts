import { injectable, inject } from "inversify";
import { Logger } from "../components/logger/logger";

@injectable()
export class MenuService {
  constructor(
    @inject(Symbol.for('Logger')) private logger: Logger) {
  }

  list = (limit: any = 10, offset: any = 0) => {
    return [];
  }

  all = () => {
    return [];
  }

  edit = (id) => {
    return [];
  }

  create = (body: any) => {
    return [];
  }

  update = (body: any) => {
    return [];
  }

  destroy = (id: any) => {
    return [];
  }
}