import { injectable, inject } from "inversify";
import { Logger } from "../components/logger/logger";
import * as md5 from "md5";
import * as jwt from "jsonwebtoken";
import * as sequelize from "sequelize";
import { DH_NOT_SUITABLE_GENERATOR } from "constants";
const _user = { 'email': 'admin@admin.com', 'pwd': '123456789' }
const JWT_KEY = (process.env.JWT_KEY) ? process.env.JWT_KEY : "12345";
@injectable()
export class AuthenticateService {
  constructor(
    @inject(Symbol.for('Logger')) private logger: Logger) {
  }
  private generateWebToken = (user, nav) => {
    return jwt.sign({
      //exp: Math.floor(Date.now() / 1000) + (60 * 60),
      data: {
        user_name: user.usu_email,
        navbar: nav
      }
    }, JWT_KEY, {
      expiresIn: '1d' // expira em 1 dia
    });
  }


  public authenticate = (user: string, pwd: string) => new Promise((resolve, reject) => {
    // tslint:disable-next-line: curly
    if (user == _user.email && pwd == _user.pwd)
      if (user != null) {
        let nav = [
          {
            codigo: 1,
            nome: 'Menu',
            link: '/menu',
            icon: '',
            inserir: true,
            alterar: true,
            listar: true,
            deletar: true,
            childrens: []
          }
        // tslint:disable-next-line: semicolon
        ]
        resolve({
          valid: true, data: {
            user: user,
            navbar: nav,
            token: this.generateWebToken(user, nav)
          }
        });
      } else {
        reject({ valid: false, data: { user: null, token: null } });
      }
  })
}
