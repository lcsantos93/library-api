import { injectable, inject } from "inversify";
import { Logger } from "../components/logger/logger";
import { BookListResponse } from "../models/imports/models"

@injectable()
export class StatusService {
    constructor(
        @inject(Symbol.for('Logger')) private logger: Logger) {
    }

    public consult = (reqStatus: BookListResponse) => {
        if (reqStatus) {
            try {
                // this.logger.info(`-- METHOD: STATUS: -- File has Content System Info>>${JSON.stringify(SystemInfoFile)}`);
                // this.logger.info(`-- THIS IS LAST-VERSION >>, ${SystemInfoFile.application.module[0].version}`);
                // this.logger.info(`-- POS ACTUAL VERSION >> ${reqStatus.status.application.module[0].version}`);
                // let posVersion = reqStatus.status.application.module[0].version;
                // let lastVersion = SystemInfoFile.application.module[0].version;

            } catch (err) {
                this.logger.error(`Error ao tentar acessar o arquivo com as informações da versão atual ${JSON.stringify(err)}`);
            }
        } else {//NOTE: 
            return false;
        }
    };
}