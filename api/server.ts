﻿import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import "reflect-metadata";
import { Container } from "inversify";
import { interfaces, InversifyExpressServer, TYPE } from "inversify-express-utils";
//NOTE: IMPORT VARIAVEIS DE AMBIENTE
import { ConfigProcess } from "./config/config"; new ConfigProcess();
import * as helmet from 'helmet';
import * as OpenApiValidatorModule from 'express-openapi-validator';
let OpenApiValidator = OpenApiValidatorModule.OpenApiValidator;
import * as appRoot from 'app-root-path';
//Component
import { Logger } from "./components/logger/logger";
//Services

import { BAD_REQUEST, OK } from "http-status";
import { AdministrativeController } from "./controllers/administrativeController";
import { AuthTransactionMiddleware } from "./middlewares/authTransactionMiddleware";
import { MongoDb } from "./components/mongodb/mongodb";
import { StatusService } from "./services/statusService";
import { BookService } from "./services/bookService";
import { UserService } from "./services/userService";
import { upload } from "./middlewares/uploadMiddleware";
import { BookResponse } from "./models/imports/models";

let logger = new Logger();
const container = new Container();

//INJECAO DE DEPENDENCIAS DE CONTROLADORES SWAGGER
container.bind<interfaces.Controller>(TYPE.Controller)
    .to(AdministrativeController).inSingletonScope().whenTargetNamed(AdministrativeController.TARGET_NAME);


//Components
container.bind<Logger>(Symbol.for('Logger')).to(Logger);
container.bind<MongoDb>(Symbol.for('MongoDb')).to(MongoDb);

//Services
container.bind<StatusService>(Symbol.for('StatusService')).to(StatusService);
container.bind<BookService>(Symbol.for('BookService')).to(BookService);
container.bind<UserService>(Symbol.for('UserService')).to(UserService);
// container.bind<Bcrypt>(Symbol.for('Bcrypt')).to(Bcrypt);

import { AuthController } from "./controllers/AuthController";
import { AuthenticateService } from "./services/AuthenticateService";

import { MenuService } from "./services/MenuService";
import { MenuController } from "./controllers/MenuController";


container.bind<interfaces.Controller>(TYPE.Controller)
    .to(AuthController).inSingletonScope().whenTargetNamed(AuthController.TARGET_NAME);

container.bind<AuthTransactionMiddleware>(Symbol.for('AuthTransactionMiddleware')).to(AuthTransactionMiddleware);
container.bind<interfaces.Controller>(TYPE.Controller)
    .to(MenuController).inSingletonScope().whenTargetNamed(MenuController.TARGET_NAME);

container.bind<AuthenticateService>(Symbol.for('AuthenticateService')).to(AuthenticateService);
    container.bind<MenuService>(Symbol.for('MenuService')).to(MenuService);
    
    
const server = new InversifyExpressServer(container);
server.setConfig((app: any) => {
    app.use('/api/docs', express.static(__dirname + '/swagger/swagger-ui.html'));
    app.use('/api-docs', express.static(__dirname + '/swagger'));
    app.use(require("morgan")("combined", { "stream": logger.stream }));
    app.use('/api-docs', express.static(__dirname + '/swagger'));
    app.use('/logs', express.static(`${appRoot}/logs`));
    app.use('/api-docs/swagger/assets', express.static('node_modules/swagger-ui-dist'));


    //NOTE: Rota de upload de arquivo de versionamento
    // app.use('/file_system/version/', express.static(process.env.FILE_SYSTEM_UPDATE));

    app.use(cors());
    app.options('*', cors());
    app.use(helmet());
    app.use(helmet.noCache());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.disable('x-powered-by')
    // new OpenApiValidator({
    //     apiSpec: __dirname + '/swagger/swagger.json'
    // }).install(app);

});

server.setErrorConfig((app: any) => {
    app.use((err: Error, request: express.Request, response: express.Response, next: express.NextFunction) => {
        let req = Object.assign({}, request.body);
        if (process.env.NODE_ENV == "development")
            console.log("StakTracer:", err.stack);
        logger.logger.error(`Requisição inválida ${JSON.stringify(req)} ${err.name} : ${err.message}`);

        req.status = 'denied'
        req.errors = {
            "code": 11,
            "category": 30,
            "message": "Requisição inválida",
            "customer-message": "Requisição inválida"
        }
        response.status(BAD_REQUEST).send(req);
    });
});

const app = server.build();
let port = (process.env.API_PORT) ? process.env.API_PORT : '3000';
let host = (process.env.API_URL) ? process.env.API_URL : 'localhost';
export async function start(): Promise<void> {
    app.listen(port, () => {
        console.log(`Start: ${host}`);
        console.log(`Listen port ${port}!`);
        console.log('Swagger /api/docs');
    });
}

