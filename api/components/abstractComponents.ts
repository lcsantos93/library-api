import { injectable } from "inversify";

export interface IStatusComponents {
    name:string;
    version:string;
    metadados?: any   
}

/*
 *    @name AbstractComponents
 *    @description Classe para extensão para metdos de monitorHealth  
 */
@injectable()
export abstract class AbstractComponents {
    abstract statusComponents:IStatusComponents;
    constructor(){}
    //NOTE: Metodo para verificar status do component
    abstract status = () => {}
}