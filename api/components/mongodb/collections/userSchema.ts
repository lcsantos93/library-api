import * as mongoose from 'mongoose';
const { Schema } = mongoose;
export const UserSchema = new Schema({
  "name": {
    type: String,
    required: true
  },
  "age": {
    type: String,
    required: true
  },
  "phone": {
    type: String,
    required: true
  },  
  "email": {
    type: String,
    required: true
  },
  "password": {
    type: String,
    required: true
  }
  
}, { collection: "Users" });

UserSchema.set('toJSON', {
  transform: function (doc, ret) {
    delete ret.__v;
  }
});