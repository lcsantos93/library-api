import * as mongoose from 'mongoose';
const { Schema } = mongoose;
export const BookSchema = new Schema({
  "title": {
    type: String,
    required: true
  },
  "isbn": {
    type: String,
    required: true
  },
  "category": {
    type: String,
    required: true
  },  
	"year": {
    type: String,
    required: true
  }
  
}, { collection: "Books" });

BookSchema.set('toJSON', {
  transform: function (doc, ret) {
    delete ret.__v;
  }
});