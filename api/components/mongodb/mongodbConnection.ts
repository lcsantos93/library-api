import * as mongoose from 'mongoose';
const user: string = process.env.MONGODB_USER;
const pwd: string = process.env.MONGODB_PASS;
const host: string = process.env.MONGODB_HOST;
const port: string = process.env.MONGODB_PORT;
const database: string = process.env.MONGODB_DATABASE;
const mongodbURL =
  (user && pwd)
    ? `mongodb://${user}:${pwd}@` +
    `${host}:${port}/${database}`
    : `mongodb://${host}:${port}/${database}`;

export class MongoDbConnection {
  private count = 0;
  private static isConnected: boolean = false;
  private static db: any = mongoose;
  constructor() { }

  public static getConnection(result: (connection) => void) {
    if (this.isConnected) {
      return result(this.db);
    } else {
      this.connect((error, db: any) => {
        return result(this.db);
      });
    }
  }

  private static connect(result: (err, connection) => void) {
    try {
      mongoose.connect(mongodbURL, { useNewUrlParser: true });
      mongoose.connection.on('error', (error) => {
        console.log('Mongo ERROR');
        console.error(error);
        this.db = false;
        // process.exit(1);
      });
      mongoose.connection.once('open', () => {
        console.log("Conexao realizada com sucesso!");
      }); ''
      mongoose.connection.on('disconnected', () => {
        console.log("Disconnected!");
        this.db = false;
        //process.exit(1);
      });

      mongoose.connection.on('close', function (ref) {
        console.log('close connection.');
      });
      this.isConnected = true;
    } catch (err) {
      //process.exit();
      this.db = false;
      throw err;
    }
  }
}
