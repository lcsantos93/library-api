/*
    Classe responsavel por importar as models do mongose
*/
import { MongoDbConnection } from './mongodbConnection';
import { injectable } from "inversify";

import { BookSchema } from './collections/bookSchema';
import { UserSchema } from './collections/UserSchema';
import { AbstractComponents, IStatusComponents } from '../abstractComponents';

@injectable()
export class MongoDb extends AbstractComponents {
  statusComponents: IStatusComponents = {
    name: "MongoDb",
    version: "1.0"
  };
  private db: any = {};
  constructor() {
    super()
    MongoDbConnection.getConnection((conn: any) => {
      this.db = conn;
      this.includeModels();
    });
  }
  //NOTE: Adicionar schemas 
  private includeModels = () => {
    this.db.model("Books", BookSchema);
    this.db.model("Users", UserSchema);
  }
  public getCollection(name = "") {
    return this.db.model(name);
  }

  //#region  Status 
  public status = () => {
    if (this.db) {
      this.statusComponents.metadados.push({ connection: true });
    } else {
      this.statusComponents.metadados.push({ connection: false });
    }
    return this.statusComponents;
  }
  //#endregion
} 