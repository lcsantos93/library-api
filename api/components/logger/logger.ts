const { createLogger, format, transports, winston } = require('winston');
const { combine, timestamp, label, printf } = format;
const DailyRotateFile = require('winston-daily-rotate-file');
import * as moment from 'moment';
import * as appRoot from 'app-root-path';
import * as fs from 'fs';
import { injectable } from 'inversify';
import { AbstractComponents, IStatusComponents } from '../abstractComponents';
const customLogTimeStamp = () => moment().format('YYYY-MM-DD HH:mm:ss.SSS');
const customLogFormater =  printf(({ level, message, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});



@injectable()
export class Logger extends AbstractComponents{
	statusComponents: IStatusComponents = {
		name: "Logger",
		version: "1.0"
	}
	options:any = {
		dailyRotateFile:{
			datePattern: "YYYY-MM-DD",
			filename: `${appRoot}/logs/app-%DATE%.log`,
			json: false,
			localTime: true,
			level: 'silly',
			timestamp: customLogTimeStamp
		},
		dailyRotateFileServer:{
			datePattern: "YYYY-MM-DD",
			filename: `${appRoot}/logs/server-%DATE%.log`,
			json: false,
			localTime: true,
			level: 'silly',
			timestamp: customLogTimeStamp
		} 
	}; 
	logger = createLogger({
		format: combine(
			timestamp(),
			customLogFormater
		),
		transports: [
			new transports.Console(),
			new transports.DailyRotateFile(this.options.dailyRotateFile)
		]
	});

	loggerServer;
	
	stream = {
		write:(message) => {
			this.loggerServer.info(message);
		}
	};

  constructor(){
	    super();
		if(!fs.existsSync(`${appRoot}/logs`)){
			fs.mkdirSync(`${appRoot}/logs`); 
		}
		

		
		this.loggerServer =  createLogger({
			format: combine(
				timestamp(),
				customLogFormater
			),
			transports: [
				new transports.Console(),
				new transports.DailyRotateFile(this.options.dailyRotateFileServer)
			]
		});
		
				
		if (process.env.NODE_ENV !== 'production') {
			this.logger.add(new transports.Console({
				format: format.simple()
			}));
		}
	}

	info(message){
		this.logger.info(message);
	}
	
	warn(message){
		this.logger.warn(message);
	}
	
	error(message){
		this.logger.error(message);
	}

	trace(err, message){
		this.logger.warn(err, message);
	}

	public status = () =>{
		//NOTE: Verificar a quantidade de files 
		return this.statusComponents;
	}
}