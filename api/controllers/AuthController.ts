﻿import * as express from "express";
import { interfaces, controller, httpPost, httpGet} from "inversify-express-utils";
import { inject, id } from "inversify";
import { Logger } from "../components/logger/logger";
import { BAD_REQUEST, OK, INTERNAL_SERVER_ERROR } from "http-status";
import { Auth } from "../models/imports/auth";
import { AuthenticateService } from "../services/AuthenticateService";



/*
* @class AuthController
* @description Controller para utilização da api 
*/

@controller("/api")
export class AuthController implements interfaces.Controller {
    public static TARGET_NAME: string = "Auth";
    constructor(@inject(Symbol.for('Logger')) private logger: Logger,
    @inject(Symbol.for('AuthenticateService')) private service: AuthenticateService) {}

    @httpPost("/authenticate")
    public  async auth(request: express.Request, response: express.Response, next: express.NextFunction) {
        this.logger.info(`Begin method: ${request.url}`);
        let req: Auth = <Auth>request.body;
        await this.service.authenticate(req.user, req.password)
         .then(s =>  response.status(OK).json(s))
         .catch(e => response.status(BAD_REQUEST).json(e));
    }
}