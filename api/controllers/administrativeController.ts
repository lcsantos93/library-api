﻿import * as express from "express";
import { interfaces, controller, httpPost, httpGet } from "inversify-express-utils";
import { inject } from "inversify";
import { Logger } from "../components/logger/logger";
import { BAD_REQUEST, OK, INTERNAL_SERVER_ERROR, NO_CONTENT } from "http-status";
import { BookRequest, BookResponse } from "../models/imports/models";
import { StatusService } from "../services/statusService";
import { BookService } from "../services/bookService";
import { UserService } from "../services/userService";
;
/*
* @class AdministrativeController
* @description Controller para utilização da api 
*/
@controller("/api")
export class AdministrativeController implements interfaces.Controller {
  public static TARGET_NAME: string = "Administrative";
  constructor(@inject(Symbol.for('Logger')) private logger: Logger,
    @inject(Symbol.for("StatusService")) private serviceStatus: StatusService,
    @inject(Symbol.for('BookService')) private bookService: BookService,
    @inject(Symbol.for('UserService')) private userService: UserService
    
  ) {
  }
  
  @httpPost("/book")
  public async adding(request: express.Request, response: express.Response, next: express.NextFunction) {
    this.logger.info(`Begin method: ${request.url}`);
    await this.bookService.addBook(request).then((res: any) => {
      response.status(OK).json(res.books);
    }) 
    next();
  }

  @httpGet("/book")
  public async listing(request: express.Request, response: express.Response, next: express.NextFunction) {
    this.logger.info(`Begin method: ${request.url}`);
    await this.bookService.listBooks(response).then((res: any) => {
      response.status(OK).json(response['books']);
    })
    next();
  }

  @httpPost("/user")
  public async add(request: express.Request, response: express.Response, next: express.NextFunction) {
    this.logger.info(`Begin method: ${request.url}`);
    await this.userService.addUser(request).then((res: any) => {
      response.status(OK).json(res.users);
    }) 
    next();
  }

  @httpGet("/user")
  public async list(request: express.Request, response: express.Response, next: express.NextFunction) {
    this.logger.info(`Begin method: ${request.url}`);
    await this.userService.listUsers(response).then((res: any) => {
      response.status(OK).json(response['users']);
    })
    next();
  }

  @httpPost("/status")
  public status(request: express.Request, response: express.Response, next: express.NextFunction) {
    this.logger.info(`Begin method: ${request.url}`);
    let req: BookRequest = <BookRequest>request.body;

  }
}       