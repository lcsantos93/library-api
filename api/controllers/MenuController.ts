﻿import * as express from "express";
import { interfaces, controller, httpPost, httpGet, httpDelete } from "inversify-express-utils";
import { inject, id } from "inversify";
import { Logger } from "../components/logger/logger";
import { BAD_REQUEST, OK } from "http-status";
import { MenuService } from "../services/MenuService";
@controller("/api")
export class MenuController implements interfaces.Controller {
    public static TARGET_NAME: string = "Menu";
    constructor(@inject(Symbol.for('Logger')) private logger: Logger,
        @inject(Symbol.for('MenuService')) private service: MenuService) { }

    @httpGet("/menu", Symbol.for('AuthTransactionMiddleware'))
    public async get(request: express.Request, response: express.Response, next: express.NextFunction) {
        this.logger.info(`Begin method: ${request.url}`);
        let params = request.query;
        response.status(200).json({valid:true, data:[]})
    }

    @httpGet("/menu/all", Symbol.for('AuthTransactionMiddleware'))
    public async getList(request: express.Request, response: express.Response, next: express.NextFunction) {
        this.logger.info(`Begin method: ${request.url}`);
        let params = request.query;
        response.status(200).json({valid:true, data:[]})
    }

    @httpGet("/menu/edit/:id", Symbol.for('AuthTransactionMiddleware'))
    public async edit(request: express.Request, response: express.Response, next: express.NextFunction) {
        this.logger.info(`Begin method: ${request.url}`);
        let params = request.params;
        response.status(200).json({valid:true, data:[]})
    }

    @httpPost("/menu/save", Symbol.for('AuthTransactionMiddleware'))
    public async save(request: express.Request, response: express.Response, next: express.NextFunction) {
        this.logger.info(`Begin method: ${request.url}`);
        let body = request.body;
        if (body.men_codigo != null) {
            body['men_ativo'] = (body['men_ativo'] == true || body['men_ativo'] == 'true') ? '1' : '0';
            await this.service.update(body);
        } else {
            delete body['men_codigo'];
            body['men_ativo'] = (body['men_ativo'] == true || body['men_ativo'] == 'true') ? '1' : '0';
            await this.service.create(body);
        }
    }


    @httpDelete("/menu/:id", Symbol.for('AuthTransactionMiddleware'))
    public async destroy(request: express.Request, response: express.Response, next: express.NextFunction) {
        this.logger.info(`Begin method: ${request.url}`);
        let id = request.params.id;
        await this.service.destroy(id);
    }
}