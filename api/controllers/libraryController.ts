import * as express from "express";
import { interfaces, controller, httpPost, httpGet} from "inversify-express-utils";
import { inject } from "inversify";
import { Logger } from "../components/logger/logger";
import { BAD_REQUEST, OK, INTERNAL_SERVER_ERROR, NO_CONTENT } from "http-status";

/*
* @class LibraryController
* @description Controller para utilização da api 
*/
@controller("/api")
export class LibraryController implements interfaces.Controller {
    public static TARGET_NAME: string = "Library";
    constructor(@inject(Symbol.for('Logger')) private logger: Logger)
        {
    }
    //not implement 
    @httpPost("/books")
    public async activation(request: express.Request, response: express.Response, next: express.NextFunction) {
        this.logger.info(`Begin method: ${request.url}`);
    }

    @httpPost("/users")
    public status(request: express.Request, response: express.Response, next: express.NextFunction) {
        this.logger.info(`Begin method: ${request.url}`);
    } 
}       