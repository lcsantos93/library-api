
/*
 @class AuthService
 @description Classe utilizada para validação de usuário 
*/
import * as appRoot from 'app-root-path';
import express = require('express');
const TERMINAL_NOT_FOUND = 'Terminal não cadastrado'
const SERIAL_NOT_FOUND = 'Serial não cadastrado'
const IMEI_NOT_FOUND = 'IMEI não cadastrado'
const ESTABLISHMENT_NOT_FOUND = 'Estabelecimento não cadastrado'
const MAC_NOT_FOUND = 'MAC não cadastrado'
import {UNAUTHORIZED} from "http-status";
import { Logger } from '../components/logger/logger';
import { inject, injectable } from 'inversify';
import { BaseMiddleware } from 'inversify-express-utils';
import * as jwt from 'jsonwebtoken';
const JWT_KEY = (process.env.JWT_KEY) ? process.env.JWT_KEY : "12345"; 
@injectable()
export class AuthTransactionMiddleware extends BaseMiddleware  {
    @inject(Symbol.for("Logger")) private logger: Logger;
    public handler(request: express.Request, response: express.Response, next: express.NextFunction){
        var token = request.headers['x-access-token'];
        if (!token) return response.status(401).send({ auth: false, message: 'No token provided.' });
        jwt.verify(token, JWT_KEY, function(err, decoded) {
          if (err) {
              return response.status(401).send({ auth: false, message: 'Failed to authenticate token.' });
          }
          request['user'] = decoded;
          return next();
        });
    }
}    