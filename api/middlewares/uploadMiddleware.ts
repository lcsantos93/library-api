
/*
 @class 
 @description 
*/
import express = require('express');
import * as multer from 'multer';
import * as appRoot from 'app-root-path';
import * as fs from 'fs';
import * as moment from 'moment';

const pathFile = () => {
    if(!fs.existsSync(`${appRoot}/uploads`)){
        fs.mkdirSync(`${appRoot}/uploads`); 
    }
    if(!fs.existsSync(`${appRoot}/uploads/${moment().format('YYYY-MM-DD')}`)){
        fs.mkdirSync(`${appRoot}/uploads/${moment().format('YYYY-MM-DD')}`); 
    }
   return `${appRoot}/uploads/${moment().format('YYYY-MM-DD')}`;
}

const storageLogs = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `${pathFile()}`)
    },
    filename: function (req, file, cb) {
        let terminalid = req.body;
        cb(null, `log-${terminalid.terminal.id}.${file.originalname.split('.').pop()}`)
    }
});

export const upload = (file:string) => multer({ storage: storageLogs }).single(file);

