import os
import subprocess

print ("\nStopping services ...\n")
subprocess.check_call(["docker-compose", "-f", "docker-compose.debug.yml", "down"])

print ("\nBuilding services ...\n")
subprocess.check_call(["docker-compose", "-f", "docker-compose.debug.yml", "build"])

print ("\nStarting services ...\n")
subprocess.check_call(["docker-compose", "-f", "docker-compose.debug.yml", "up", "-d", "--force-recreate"])
