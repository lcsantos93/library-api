### Instalação
Primeiramente vamos instalar as dependências do projeto, executar no terminal o comando abaixo:

```sh
npm install
```
Agora iremos [transpilar](https://en.wikipedia.org/wiki/Source-to-source_compiler) o projeto e gerar a versão de distruibição.
```sh
npm run build
```

### Execução
A partir de agora para executar o projeto execute o comando abaixo
```sh
npm start
```
