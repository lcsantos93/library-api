module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  moduleNameMapper: {
    '^@App/(.*)$': '<rootDir>/test/$1',
    '^lib/(.*)$': '<rootDir>/test/$1'
  }
};